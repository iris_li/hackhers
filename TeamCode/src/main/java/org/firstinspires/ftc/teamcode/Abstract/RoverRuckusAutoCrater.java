package org.firstinspires.ftc.teamcode.Abstract;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
// IGNORE THIS FOR NOW!
@Autonomous(name = "RoverRuckusCrater")
public class RoverRuckusAutoCrater extends LinearOpMode {
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    Orientation lastAngles = new Orientation();
    GoldAlignDetector detector;
    double globalAngle, correction;
    RobotHardware robot;
    IMU imu;

    @Override
    public void runOpMode() throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        rightfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        detector = new GoldAlignDetector();
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        detector.useDefaults();
        robot = new RobotHardware(hardwareMap);
        imu = new IMU(hardwareMap);

        detector.enable();
        robot.setDropperPos(0);
        while(!opModeIsActive()) {
            robot.setLiftPower(0);
        }
        //angle between minerals is ~25 degrees
        waitForStart();
        if(opModeIsActive()) {
            telemetry.addLine("Deploying from lander!");
            telemetry.update();
            //deploy the lander
           /* robot.setLiftPower(-1);
            sleep(1000);
            robot.setLiftPower(1);
            sleep(500);
            driveStraight(.35,500);
            robot.setLiftPower(-.6);
            sleep(1000);
            robot.setLiftPower(0);
            */
            driveStraight(.3,1450);
            telemetry.addLine("Searching for gold...");
            telemetry.update();
            //start from the left
            rotate(-30);
            sleep(500);
            if(detector.getAligned()) {
                telemetry.addLine("Detected first");
                telemetry.update();
                driveStraight(.6,1800);
                driveStraight(-.6,800);
                rotate(-40);
                driveStraight(.6,2100);
                rotate(-65);
                driveStraight(.6,3200);
            }else{
                //rotate +25 degrees, check.
                rotate(30);
                sleep(500);
                if(detector.getAligned()){
                    telemetry.addLine("Detected second");
                    telemetry.update();
                    driveStraight(.6,1500);
                    driveStraight(-.6,800);
                    rotate(-90);
                    driveStraight(.6,3000);
                    rotate(-45);
                    driveStraight(.6,2600);
                }else{
                    rotate(30);
                    telemetry.addLine("Detected third/didn't see");
                    telemetry.update();
                    driveStraight(.6,1800);
                    driveStraight(-.6,1000);
                    rotate(-110);
                    driveStraight(.6,3300);
                    rotate(-45);
                    driveStraight(.6,2600);
                }
            }
            telemetry.addLine("Claiming depot!");
            telemetry.update();
            robot.setDropperPos(.45);
            sleep(2000);
            robot.setDropperPos(0);
            telemetry.addLine("~Parking on crater~");
            telemetry.update();
            driveStraight(-.8,3300);
            //rotate(15);
            //driveStraight(-.8,1000);
            detector.disable();
        }
    }
    //use in a loop
    //finds the difference in last angle and current angle.
    public double trackRotation(){
        Orientation angles = imu.getZYXAngles();
        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;
        //range is -180 to 180 (- is left, + is right)
        if (deltaAngle < -180) {
            deltaAngle += 360;
        }
        else if(deltaAngle > 180) {
            deltaAngle -= 360;
        }
        globalAngle = deltaAngle;
        //lastAngles = angles;
        return globalAngle;
    }
    public void resetAngle() {
        lastAngles = imu.getZYXAngles();
        globalAngle = 0;
    }
    //left +, right - vals
    public void rotate(double degrees){
        resetAngle();
        double delta = Math.abs(trackRotation());
        //double lastAngle = imu.getAngleZ();
        //double currentAngle = imu.getAngleZ();
        if(degrees > 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnLeft(.3);
                delta = Math.abs(trackRotation());
            }
        }else if(degrees < 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnRight(.3);
                delta = Math.abs(trackRotation());
            }
        }
        lastAngles = imu.getZYXAngles();
        robot.drive(0);
    }
    //corrects driving in a straight line by adjustDirection's correction
    public void driveStraight(double power,long milliseconds){
        ElapsedTime timer = new ElapsedTime();
        timer.reset();
        resetAngle();
        while ((timer.milliseconds() < milliseconds) && opModeIsActive()) {
            correction = adjustDirection();
            robot.setRightMotorPwr(power);
            robot.setLeftMotorPwr(power + correction);
        }
        robot.drive(0);
    }
    //if the delta angle is 0, no correction. else, correction = -deltaAngle*gain (which could be pos or neg)
    public double adjustDirection() {
        double correction, angle, gain = 0.10;
        angle = trackRotation();
        if (angle == 0) {
            correction = 0;
        } else {
            correction = -angle;
        }
        correction = correction * gain;
        return correction;
    }
    public void drive(double power, long time) {
        leftfront.setPower(power);
        leftback.setPower(power);
        rightfront.setPower(power);
        rightback.setPower(power);
        sleep(time);
    }
    public void liftrobo(double power, long time) {
        robot.setLiftPower(power);
        sleep(time);
    }

}

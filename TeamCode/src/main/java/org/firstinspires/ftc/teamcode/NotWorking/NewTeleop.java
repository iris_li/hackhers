package org.firstinspires.ftc.teamcode.NotWorking;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.NotWorking.CoreOp;

/**
 * Created by Iris and Phoebe on 2/13/2018.
 */
@TeleOp (name = "Teleop")
@Disabled
public class NewTeleop extends CoreOp {
    @Override
    public void init() {
    }
    public void loop(){
        // left stick controls direction
        // right stick X controls rotation
        float gamepad1LeftY = gamepad1.left_stick_y;
        float gamepad1LeftX = -gamepad1.left_stick_x;
        float gamepad1RightX = -gamepad1.right_stick_x;

        /*

        double pivotPositionB = 0;
        double pivotPositionM = .7;

        if(gamepad1.dpad_up){
            arm.pivot(false,bjoint);
        }
        else if(gamepad1.dpad_down){
            arm.pivot(true,bjoint);
        }

        if(gamepad1.dpad_left){
            arm.pivot(false,mjoint);
        }
        else if(gamepad1.dpad_right){
            arm.pivot(true,mjoint);
        }

        if(gamepad1.a){
            arm.spin(.3,intake);
        }
        else if(gamepad1.b){
            arm.spin(-.3,intake);
        }
        */

        // holonomic formulas for omni wheels
            /*
            float FrontLeft = -gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
            float FrontRight = gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
            float BackRight = gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
            float BackLeft = -gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
            */

        // formulas for mecanum wheels [requires changing of values and testing]
            /*inputs: y, x, and r
            flPower = + y + x + r
            frPower = + y - x - r
            brPower = + y + x - r
            blPower = + y - x + r
            */
        float FrontLeft = gamepad1LeftY + gamepad1LeftX + gamepad1RightX;
        float FrontRight = gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
        float BackRight = gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
        float BackLeft = gamepad1LeftY - gamepad1LeftX + gamepad1RightX;

        // clip the right/left values so that the values never exceed +/- 1
        FrontRight = Range.clip(FrontRight, -1, 1);
        FrontLeft = Range.clip(FrontLeft, -1, 1);
        BackLeft = Range.clip(BackLeft, -1, 1);
        BackRight = Range.clip(BackRight, -1, 1);

        // write the values to the motors
        rightfront.setPower(FrontRight);
        leftfront.setPower(FrontLeft);
        leftback.setPower(BackLeft);
        rightback.setPower(BackRight);

		/*
		 * Telemetry for debugging
		 */
        telemetry.addData("Text", "*** Robot Data***");
        telemetry.addData("Joy XL YL XR",  String.format("%.2f", gamepad1LeftX) + " " +
                String.format("%.2f", gamepad1LeftY) + " " +  String.format("%.2f", gamepad1RightX));
        telemetry.addData("f left pwr",  "front left  pwr: " + String.format("%.2f", FrontLeft));
        telemetry.addData("f right pwr", "front right pwr: " + String.format("%.2f", FrontRight));
        telemetry.addData("b right pwr", "back right pwr: " + String.format("%.2f", BackRight));
        telemetry.addData("b left pwr", "back left pwr: " + String.format("%.2f", BackLeft));
    }

    @Override
    public void stop() {
    }

    /*
     * This method scales the joystick input so for low joystick values, the
     * scaled value is less than linear.  This is to make it easier to drive
     * the robot more precisely at slower speeds.
     */
    double scaleInput(double dVal)  {
        double[] scaleArray = { 0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
                0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00 };

        // get the corresponding index for the scaleInput array.
        int index = (int) (dVal * 16.0);

        // index should be positive.
        if (index < 0) {
            index = -index;
        }

        // index cannot exceed size of array minus 1.
        if (index > 16) {
            index = 16;
        }

        // get value from the array.
        double dScale = 0.0;
        if (dVal < 0) {
            dScale = -scaleArray[index];
        } else {
            dScale = scaleArray[index];
        }

        // return scaled value.
        return dScale;
    }
}

package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.AutonomousTransitionSamples.AutoTransitioner;

/**
 * Blue autonomous for lowering robot and parking in the crater, imaging and servos coming soon
 */
@Autonomous (name="NoCVDepotAuto")

public class NoCVDepotAuto extends LinearOpMode {
    // gotta have motors
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    Servo dropper;
    DcMotor lift;

    public void runOpMode()throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        dropper = hardwareMap.servo.get("dropper");
        lift = hardwareMap.dcMotor.get("lift");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        //sets motors to stop moving when they don't receive any inputs, prevents drifting
        rightfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

       /* GoldAlignDetector g = new GoldAlignDetector();
        g.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        g.useDefaults();
        g.enable();
        */
        lift.setPower(0);
        dropper.setPosition(0);
        AutoTransitioner.transitionOnStop(this,"BasicTeleopDrive");
        waitForStart();
        if(opModeIsActive()) {
            dropper.setPosition(0);
            drive(.5, 3500);
            turnLeft(.5,400);
            //drive(.5,500);
            // dropper here
            dropper.setPosition(.45);
            // gives some time to drop the marker
            sleep(1000);
            dropper.setPosition(0);
            // proceed to crater
            drive(-.7, 2500);
            turnLeft(.5,200);
            drive(-.8,1000);
        }
    }

        //basic functions to be used
        public void drive(double power, long time) {
            leftfront.setPower(power);
            leftback.setPower(power);
            rightfront.setPower(power);
            rightback.setPower(power);
            sleep(time);
        }
        public void turnLeft(double power, long time){
            rightfront.setPower(-power);
            rightback.setPower(-power);
            leftfront.setPower(power);
            leftback.setPower(power);
            sleep(time);
        }
        public void turnRight(double power, long time){
            rightfront.setPower(power);
            rightback.setPower(power);
            leftfront.setPower(-power);
            leftback.setPower(-power);
            sleep(time);
        }
        public void strafeRight(double power, long time){
            rightfront.setPower(-power);
            rightback.setPower(power);
            leftfront.setPower(power);
            leftback.setPower(-power);
            sleep(time);
        }
        public void strafeLeft(double power, long time){
            rightfront.setPower(power);
            rightback.setPower(-power);
            leftfront.setPower(-power);
            leftback.setPower(power);
            sleep(time);
        }
        public void turnRight(double power) {
            rightfront.setPower(power);
            rightback.setPower(-power);
            leftfront.setPower(-power);
            leftback.setPower(power);
        }
}

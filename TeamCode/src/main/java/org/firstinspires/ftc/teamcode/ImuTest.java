package org.firstinspires.ftc.teamcode;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import com.qualcomm.robotcore.hardware.DcMotor;
import org.firstinspires.ftc.robotcore.external.Func;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;
import java.util.Locale;

public class ImuTest extends LinearOpMode{

    //when upright, heading = 0, roll =-90, pitch = 0;
    // The IMU sensor object
    BNO055IMU imu;
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;

    // State used for updating telemetry
    Orientation angles;
    Acceleration gravity;

    Orientation lastAngles = new Orientation();
    double globalAngle, power, correction;

    public void runOpMode() {
        // Set up the parameters with which we will use our IMU. Note that integration
        // algorithm here just reports accelerations to the logcat log; it doesn't actually
        // provide positional information.
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        // Retrieve and initialize the IMU. We expect the IMU to be attached to an I2C port
        // on a Core Device Interface Module, configured to be a sensor of type "AdaFruit IMU",
        // and named "imu".
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");

        imu = hardwareMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);

        // Set up our telemetry dashboard
        composeTelemetry();
        float initialAngle = angles.firstAngle;

        waitForStart();

        while(opModeIsActive()) {
            /*
            initial angle = getAngle
            start turning
            turned angle = initial angle - getAngle
            drive forward distance
            turn(getAngle-turned angle)
             */
            telemetry.update();
            correction = adjustDirection();
            applyCorrection(.5);
            sleep(2000);
            stopMotors();
        }
    }
    public void stopMotors(){
        leftback.setPower(0);
        leftfront.setPower(0);
        rightfront.setPower(0);
        rightback.setPower(0);
    }
    public void applyCorrection(double power) {
        leftback.setPower(power + correction);
        leftfront.setPower(power + correction);
        rightfront.setPower(power);
        rightback.setPower(power);
    }
    public void resetAngle()
    {
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        globalAngle = 0;
    }
    // + angles are left, - angles are right
    public double getAngle() {
        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC,AxesOrder.ZYX,AngleUnit.DEGREES);
        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;
        if (deltaAngle < -180) {
            deltaAngle += 360;
        }
        else if(deltaAngle < 180) {
            deltaAngle -= 360;
        }
        globalAngle = deltaAngle;
        lastAngles = angles;
        return globalAngle;
    }
    public void rotate(int degrees,double power){
        double leftBackPower = 0;
        double leftFrontPower = 0;
        double rightBackPower = 0;
        double rightFrontPower = 0;

        resetAngle();

        if(degrees < 0){
            leftBackPower = -power;
            rightBackPower = power;
            leftFrontPower = -power;
            rightFrontPower = power;
        }
        else if(degrees > 0){
            leftBackPower = power;
            rightBackPower = -power;
            leftFrontPower = power;
            rightFrontPower = -power;
        }

        leftfront.setPower(leftBackPower);
        leftback.setPower(leftFrontPower);
        rightfront.setPower(rightBackPower);
        rightback.setPower(rightFrontPower);

        if(degrees < 0) {
            while(opModeIsActive() && getAngle() == 0) {}

            while(opModeIsActive() && getAngle() > degrees) {}

        }
        else {
            while (opModeIsActive() && getAngle() < degrees) {}
        }

        stopMotors();

        resetAngle();


    }

    //returns power correction value for moving in a straight line (if angle = 0)
    public double adjustDirection() {
        double correction, angle, gain = 0.10;
        angle = getAngle();
        if (angle == 0) {
            correction = 0;
        }
        else {
            correction = -angle;
        }
        correction = correction * gain;
        return correction;
    }
    public void drive(double power, long time) {
        leftfront.setPower(power);
        leftback.setPower(power);
        rightfront.setPower(power);
        rightback.setPower(power);
        sleep(time);
    }
    public void turnLeft(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(-power);
        leftfront.setPower(power);
        leftback.setPower(power);
        sleep(time);
    }
    public void turnRight(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(power);
        leftfront.setPower(-power);
        leftback.setPower(-power);
        sleep(time);
    }
    void composeTelemetry() {

        // At the beginning of each telemetry update, grab a bunch of data
        // from the IMU that we will then display in separate lines.
        telemetry.addAction(new Runnable() { @Override public void run()
        {
            // Acquiring the angles is relatively expensive; we don't want
            // to do that in each of the three items that need that info, as that's
            // three times the necessary expense.
            angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            gravity  = imu.getGravity();
        }
        });

        telemetry.addLine()
                .addData("status", new Func<String>() {
                    @Override public String value() {
                        return imu.getSystemStatus().toShortString();
                    }
                })
                .addData("calib", new Func<String>() {
                    @Override public String value() {
                        return imu.getCalibrationStatus().toString();
                    }
                });

        telemetry.addLine()
                .addData("heading", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.firstAngle);
                    }
                })
                .addData("roll", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.secondAngle);
                    }
                })
                .addData("pitch", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.thirdAngle);
                    }
                });

        telemetry.addLine()
                .addData("grvty", new Func<String>() {
                    @Override public String value() {
                        return gravity.toString();
                    }
                })
                .addData("mag", new Func<String>() {
                    @Override public String value() {
                        return String.format(Locale.getDefault(), "%.3f",
                                Math.sqrt(gravity.xAccel*gravity.xAccel
                                        + gravity.yAccel*gravity.yAccel
                                        + gravity.zAccel*gravity.zAccel));
                    }
                });
    }

    //----------------------------------------------------------------------------------------------
    // Formatting
    //----------------------------------------------------------------------------------------------

    String formatAngle(AngleUnit angleUnit, double angle) {
        return formatDegrees(AngleUnit.DEGREES.fromUnit(angleUnit, angle));
    }

    String formatDegrees(double degrees){
        return String.format(Locale.getDefault(), "%.1f", AngleUnit.DEGREES.normalize(degrees));
    }

}

package org.firstinspires.ftc.teamcode.Testing;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
//Created by Meghan on 12/13/18
@TeleOp (name = "DropperTest")
public class TeleopTempClaiming extends OpMode {
    Servo dropper;
    //cjoint = claiming joint
    public void pivot(double increment, Servo servo, double position) {
        while(servo.getPosition()!= position){
            if(servo.getPosition() < position) {
                servo.setPosition(servo.getPosition() + increment);
            }
            else if(servo.getPosition() > position) {
                servo.setPosition(servo.getPosition() - increment);
            }
        }
    }
    public void init(){

    }
    public void loop(){
        //figure out required position
        double ServoPos = Math.abs(dropper.getPosition())*.01;
        if(gamepad1.dpad_right) {
            //test the position values
            pivot(ServoPos, dropper,.7);
        }
        else{
            pivot(-ServoPos, dropper,.15);
        }
    }
}

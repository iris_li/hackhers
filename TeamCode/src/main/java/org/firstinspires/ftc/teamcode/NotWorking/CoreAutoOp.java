package org.firstinspires.ftc.teamcode.NotWorking;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Iris on 2/13/2018.
 */

public abstract class CoreAutoOp extends LinearOpMode{
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    Servo clawservo1;
    Servo clawservo2;
    CRServo crservo1;
    CRServo crservo2;
    DcMotor verticalslide;
    Servo jewelservo;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;

    public void runOpMode() throws InterruptedException{
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        verticalslide = hardwareMap.dcMotor.get("verticalslide");

        jewelservo = hardwareMap.servo.get("jewelservo");
        clawservo1 = hardwareMap.servo.get("rightclawservo");
        clawservo2 = hardwareMap.servo.get("leftclawservo");
        crservo1 = hardwareMap.crservo.get("rightcrservo");
        crservo2 = hardwareMap.crservo.get("leftcrservo");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);
        verticalslide.setDirection(DcMotorSimple.Direction.REVERSE);

        //prevents drifting
        verticalslide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // get a reference to the rev color sensor and distance sensor.
        sensorColor = hardwareMap.get(ColorSensor.class, "sensor_color_distance");
        sensorDistance = hardwareMap.get(DistanceSensor.class, "sensor_color_distance");
    }

    public void drive(double power, long time) {
        leftfront.setPower(power);
        leftback.setPower(power);
        rightfront.setPower(power);
        rightback.setPower(power);
        sleep(time);
    }
    public void turnLeft(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(-power);
        leftfront.setPower(power);
        leftback.setPower(power);
        sleep(time);
    }
    public void turnRight(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(power);
        leftfront.setPower(-power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeRight(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(power);
        leftfront.setPower(power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeLeft(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(-power);
        leftfront.setPower(-power);
        leftback.setPower(power);
        sleep(time);
    }
}

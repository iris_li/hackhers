package org.firstinspires.ftc.teamcode.HackHers1718;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by Iris on 11/15/2017.
 */
//Teleop where left joystick controls movement and right joystick controls rotation
@TeleOp(name = "BasicTeleopDrive")
    public class TeleopDrive extends OpMode{
        DcMotor motorFrontRight;
        DcMotor motorFrontLeft;
        DcMotor motorBackRight;
        DcMotor motorBackLeft;
        DcMotor lift;
        /*Servo bjoint;
        Servo mjoint;
        CRServo intake;
        CRServo tjoint;
        */
        Servo dropper;
        //CRServo spin1;
        //CRServo spin2;
        /**
         * Constructor
         */
        public TeleopDrive() {
        }
        @Override
        public void init() {
		/*
		 * Use the hardwareMap to get the dc motors and servos by name. Note
		 * that the names of the devices must match the names used when you
		 * configured your robot and created the configuration file.
		 */
            motorFrontRight = hardwareMap.dcMotor.get("rightfront");
            motorFrontLeft = hardwareMap.dcMotor.get("leftfront");
            motorBackLeft = hardwareMap.dcMotor.get("leftback");
            motorBackRight = hardwareMap.dcMotor.get("rightback");
            dropper = hardwareMap.servo.get("dropper");
            lift = hardwareMap.dcMotor.get("lift");
            //spin1 = hardwareMap.crservo.get("spin1");
            //spin2 = hardwareMap.crservo.get("spin2");

            //bjoint = hardwareMap.servo.get("bottom joint");
            /*mjoint = hardwareMap.servo.get("middle joint");
            tjoint = hardwareMap.crservo.get("top joint");
            intake = hardwareMap.crservo.get("intake");
            */

            motorFrontRight.setDirection(DcMotorSimple.Direction.REVERSE);
            motorBackRight.setDirection(DcMotorSimple.Direction.REVERSE);

            //sets motors to stop moving when they don't receive any inputs, prevents drifting
            motorFrontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            motorFrontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            motorBackLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            motorBackRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        }
        @Override
        public void loop() {
            // left stick controls direction
            // right stick X controls rotation
            float gamepad1LeftY = gamepad1.left_stick_y;
            float gamepad1LeftX = -gamepad1.left_stick_x;
            float gamepad1RightX = -gamepad1.right_stick_x;

            //scales the servo position so the closer it gets to the end bounds, the more it slows down
            //double servoPos;
            //servoPos = (1-Math.abs(mjoint.getPosition()))*.01;
            //dropper
            if (gamepad2.dpad_down) {
                dropper.setPosition(.45);
            } else {
                dropper.setPosition(0);
            }
            //lift

            if (gamepad2.a){
                lift.setPower(.6);
            } else if(gamepad2.b){
                lift.setPower(-.6);
            } else {
                lift.setPower(0);
            }

            //spinning intake
            /*if (gamepad2.x){
                spin1.setPower(.7);
                spin2.setPower(.7);
            } else if(gamepad2.y){
                spin1.setPower(-.7);
                spin2.setPower(-.7);
            }else{
                spin1.setPower(0);
                spin2.setPower(0);
            }
            */
            /*
            if(gamepad1.dpad_up){
                pivot(false, .1, bjoint);
            }
            else if(gamepad1.dpad_down){
                pivot(true, .1,bjoint);
            }
            else{
                pivot(true, 0, bjoint);
            }
            */

            /*if(gamepad2.dpad_left){
                pivot(-servoPos, mjoint);
            }
            else if(gamepad2.dpad_right){
                pivot(servoPos, mjoint);
            }
            else{
                pivot(0, mjoint);
            }

            if(gamepad2.a){
                intake.setPower(.7);
            }
            else if(gamepad2.b){
                intake.setPower(-.7);
            }
            else {
                intake.setPower(0);
            }

            if(gamepad2.x){
                tjoint.setPower(.7);
            }
            else if(gamepad2.y){
                tjoint.setPower(-.7);
            }
            else{
                tjoint.setPower(0);
            }
*/

            // holonomic formulas for omni wheels
            /*
            float FrontLeft = -gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
            float FrontRight = gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
            float BackRight = gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
            float BackLeft = -gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
            */

            // formulas for mecanum wheels [requires changing of values and testing]
            /*inputs: y, x, and r
            flPower = + y + x + r
            frPower = + y - x - r
            brPower = + y + x - r
            blPower = + y - x + r
            */
            float FrontLeft = gamepad1LeftY + gamepad1LeftX + gamepad1RightX;
            float FrontRight = gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
            float BackRight = gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
            float BackLeft = gamepad1LeftY - gamepad1LeftX + gamepad1RightX;

            // clip the right/left values so that the values never exceed +/- 1
            FrontRight = Range.clip(FrontRight, -1, 1);
            FrontLeft = Range.clip(FrontLeft, -1, 1);
            BackLeft = Range.clip(BackLeft, -1, 1);
            BackRight = Range.clip(BackRight, -1, 1);

            // write the values to the motors
            motorFrontRight.setPower(FrontRight);
            motorFrontLeft.setPower(FrontLeft);
            motorBackLeft.setPower(BackLeft);
            motorBackRight.setPower(BackRight);
		/*
		 * Telemetry for debugging
		 */
            telemetry.addData("Text", "*** Robot Data***");
            telemetry.addData("Joy XL YL XR",  String.format("%.2f", gamepad1LeftX) + " " +
                    String.format("%.2f", gamepad1LeftY) + " " +  String.format("%.2f", gamepad1RightX));
            telemetry.addData("f left pwr",  "front left  pwr: " + String.format("%.2f", FrontLeft));
            telemetry.addData("f right pwr", "front right pwr: " + String.format("%.2f", FrontRight));
            telemetry.addData("b right pwr", "back right pwr: " + String.format("%.2f", BackRight));
            telemetry.addData("b left pwr", "back left pwr: " + String.format("%.2f", BackLeft));
        }

        @Override
        public void stop() {
        }

        public void pivotTo(double position, Servo servo){
            while(servo.getPosition() < position) {
                servo.setPosition(servo.getPosition() + (position*.1));
            }
            while(servo.getPosition() > position){
                servo.setPosition(servo.getPosition() - (position*.1));
            }
        }
        //set increment to - to go in negative direction
        //To Do: make variable degrees, from servo position value. Slower pivoting when arm is lifted fully
        public void pivot(double increment, Servo servo) {
            servo.setPosition(servo.getPosition() + increment);
        }
        public void pivot(boolean posdir, double increment, Servo servo){
            if(posdir){
                servo.setPosition(servo.getPosition() + increment);
            }
            else if (!posdir){
                servo.setPosition(servo.getPosition() - increment);
            }
        }

        /*
         * This method scales the joystick input so for low joystick values, the
         * scaled value is less than linear.  This is to make it easier to drive
         * the robot more precisely at slower speeds.
         */
        double scaleInput(double dVal)  {
            double[] scaleArray = { 0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
                    0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00 };

            // get the corresponding index for the scaleInput array.
            int index = (int) (dVal * 16.0);

            // index should be positive.
            if (index < 0) {
                index = -index;
            }

            // index cannot exceed size of array minus 1.
            if (index > 16) {
                index = 16;
            }

            // get value from the array.
            double dScale = 0.0;
            if (dVal < 0) {
                dScale = -scaleArray[index];
            } else {
                dScale = scaleArray[index];
            }

            // return scaled value.
            return dScale;
        }

    }

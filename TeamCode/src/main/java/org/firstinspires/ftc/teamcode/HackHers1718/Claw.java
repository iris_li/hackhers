package org.firstinspires.ftc.teamcode.HackHers1718;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
/**
 * Created by Iris on 10/2/2017.
 */
@TeleOp (name= "Claw prototype")
@Disabled
public class Claw extends LinearOpMode {
    //declare motor
    DcMotor clawmotor;

    public void runOpMode() throws InterruptedException {
        //hardware map the claw motor
        clawmotor = hardwareMap.dcMotor.get("clawmotor");

        waitForStart();

        while(opModeIsActive()) {
            //If right bumper is pressed, claw motor increased power.
            //If left bumper is pressed, claw motor increases power in the other direction
            //If no bumper is pressed, motor stops moving
            if (gamepad2.right_bumper) {
                clawmotor.setPower(.5);
            } else if (gamepad2.left_bumper){
                clawmotor.setPower(-.5);
            } else{
                clawmotor.setPower(0);
            }
        }
    }
}

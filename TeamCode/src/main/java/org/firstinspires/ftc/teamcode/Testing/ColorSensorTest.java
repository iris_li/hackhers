package org.firstinspires.ftc.teamcode.Testing;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Iris on 11/14/2017.
 */
//for adafruit color sensor- doesn't work with rev expansion hub unless we have it custom wired
@Autonomous(name="ColorSensorTest", group="Autonomous")
@Disabled
    public class ColorSensorTest extends LinearOpMode{
        /*Pseudocoding
        Lower servo
        if colorsensor sees red
        drive backwards
        else drive forward
        lift servo*/
        /*lower servo
        if colorsensor sees blue
        drive backwards
        else drive forward
        lift servo*/
        //same for both sides of field

        DcMotor leftfront;
        DcMotor rightfront;
        DcMotor leftback;
        DcMotor rightback;
        Servo jewelservo;
        ColorSensor colorsensor;

        public void runOpMode()throws InterruptedException{
            // hsvValues is an array that will hold the hue, saturation, and value information.
            float hsvValues[] = {0F,0F,0F};
            // values is a reference to the hsvValues array.
            final float values[] = hsvValues;

            leftfront = hardwareMap.dcMotor.get("leftfront");
            leftback = hardwareMap.dcMotor.get("leftback");
            rightfront = hardwareMap.dcMotor.get("rightfront");
            rightback = hardwareMap.dcMotor.get("rightback");
            jewelservo = hardwareMap.servo.get("jewelservo");
            colorsensor = hardwareMap.colorSensor.get("colorsensor");

            rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
            rightback.setDirection(DcMotorSimple.Direction.REVERSE);

            waitForStart();
            while(opModeIsActive()){
                // convert the RGB values to HSV values.
                Color.RGBToHSV((colorsensor.red() * 255) / 800, (colorsensor.green() * 255) / 800, (colorsensor.blue() * 255) / 800, hsvValues);

                telemetry.addData("Red", colorsensor.red());
                telemetry.addData("Green", colorsensor.green());
                telemetry.addData("Blue", colorsensor.blue());
                telemetry.addData("Hue", hsvValues[0]);
                telemetry.update();

            /*if (colorsensor.red() < && colorsensor.green()< && colorsensor.blue()< &&colorsensor.argb() < ){
                   leftfront.setPower(.7);
                   leftback.setPower(.7);
                   rightfront.setPower(.7);
                   rightback.setPower(.7);
                   wait(1000);
               }else if(colorsensor.red() > && colorsensor.green()> && colorsensor.blue()> &&colorsensor.argb() > ){
                   leftfront.setPower(-.7);
                   leftback.setPower(-.7);
                   rightfront.setPower(-.7);
                   rightback.setPower(-.7);
               }*/
            }

        }

    }

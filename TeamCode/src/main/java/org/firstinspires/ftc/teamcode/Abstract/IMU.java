package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

import java.util.Locale;

public class IMU {
    BNO055IMU imu;
    Orientation angles = new Orientation();
    Acceleration gravity;
    HardwareMap hardwareMap;

    public IMU(HardwareMap hardwareMap) {
        this.hardwareMap = hardwareMap;
        imu = this.hardwareMap.get(BNO055IMU.class, "imu");
        initIMU();
    }

    public void initIMU(){
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        imu.initialize(parameters);
    }
    public Orientation getZYXAngles(){
        return imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }
    public String getAnglesString() {
        return getZYXAngles().toString();
    }
    public double getAngleX(){
        //EXTRINSIC enum remains fixed; it does not move with the robot
        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX,AngleUnit.DEGREES);
        return angles.thirdAngle;
    }
    public double getAngleY(){
        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX,AngleUnit.DEGREES);
        return angles.secondAngle;
    }
    public double getAngleZ(){
        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX,AngleUnit.DEGREES);
        return angles.firstAngle;
    }
    public String getGravityString(){
        gravity = imu.getGravity();
        return gravity.toString();
    }
    public Acceleration getGravity(){
        gravity = imu.getGravity();
        return gravity;
    }
    //public double getXVelocity(){ }
    /*
    acceleration at one point: m/s/s
    velocity at one point: m/s
    distance at one point: m
    totalDistance: integral of distance
     */
    //public double getXVelocity(){ }
}

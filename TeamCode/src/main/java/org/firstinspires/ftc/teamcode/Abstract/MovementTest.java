package org.firstinspires.ftc.teamcode.Abstract;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Func;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

import java.util.Locale;

@Autonomous (name = "IMUMovementTest")
public class MovementTest extends LinearOpMode {
    Orientation lastAngles = new Orientation();
    GoldAlignDetector detector;
    double globalAngle, correction;
    RobotHardware robot;
    IMU imu;

    @Override
    public void runOpMode() throws InterruptedException {
        detector = new GoldAlignDetector();
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        detector.useDefaults();
        robot = new RobotHardware(hardwareMap);
        imu = new IMU(hardwareMap);
        // Optional tuning
        /*detector.alignSize = 100; // How wide (in pixels) is the range in which the gold object will be aligned. (Represented by green bars in the preview)
        detector.alignPosOffset = 0; // How far from center frame to offset this alignment zone.
        detector.downscale = 0.4; // How much to downscale the input frames

        detector.areaScoringMethod = DogeCV.AreaScoringMethod.MAX_AREA; // Can also be PERFECT_AREA
        //detector.perfectAreaScorer.perfectArea = 10000; // if using PERFECT_AREA scoring
        detector.maxAreaScorer.weight = 0.005; //

        detector.ratioScorer.weight = 5; //
        detector.ratioScorer.perfectRatio = 1.0; // Ratio adjustment
        */
        detector.enable();
        //angle between minerals is ~35-40 degrees
        waitForStart();
        if (opModeIsActive()) {
            lastAngles = imu.getZYXAngles();
            telemetry.addLine("Turning 35 degrees");
            //composeTelemetry();
            telemetry.addData("angle values",imu.getAnglesString());
            telemetry.update();
            rotate(-30);
            telemetry.addLine("Stopped");
            sleep(1000);
            //composeTelemetry();
            telemetry.addData("angle values",imu.getAnglesString());
            telemetry.update();
            sleep(2000);
            telemetry.addLine("Driving Straight");
            telemetry.update();
            driveStraight(.4,2000);
        }
    }
    //use in a loop
    //finds the difference in last angle and current angle.
    public double trackRotation(){
        Orientation angles = imu.getZYXAngles();
        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;
        //range is -180 to 180 (- is left, + is right)
        if (deltaAngle < -180) {
            deltaAngle += 360;
        }
        else if(deltaAngle > 180) {
            deltaAngle -= 360;
        }
        globalAngle = deltaAngle;
        //lastAngles = angles;
        return globalAngle;
    }
    public void resetAngle() {
        lastAngles = imu.getZYXAngles();
        globalAngle = 0;
    }
    //left +, right - vals
    public void rotate(double degrees){
        resetAngle();
        double delta = trackRotation();
        //double lastAngle = imu.getAngleZ();
        //double currentAngle = imu.getAngleZ();
        if(degrees > 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnLeft(.3);
                delta = trackRotation();
            }
        }else if(degrees < 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnRight(.3);
                delta = trackRotation();
            }
        }
        lastAngles = imu.getZYXAngles();
        robot.drive(0);
    }
    //corrects driving in a straight line by adjustDirection's correction
    public void driveStraight(double power,long milliseconds){
        ElapsedTime timer = new ElapsedTime();
        timer.reset();
        while ((timer.milliseconds() < milliseconds) && opModeIsActive()) {
            correction = adjustDirection();
            robot.setRightMotorPwr(power);
            robot.setLeftMotorPwr(power + correction);
        }
    }
    //if the delta angle is 0, no correction. else, correction = -deltaAngle*gain (which could be pos or neg)
    public double adjustDirection() {
        double correction, angle, gain = 0.10;
        angle = trackRotation();
        if (angle == 0) {
            correction = 0;
        }
        else {
            correction = -angle;
        }
        correction = correction * gain;
        return correction;
    }
    String formatAngle(AngleUnit angleUnit, double angle) {
        return formatDegrees(AngleUnit.DEGREES.fromUnit(angleUnit, angle));
    }

    String formatDegrees(double degrees){
        return String.format(Locale.getDefault(), "%.1f", AngleUnit.DEGREES.normalize(degrees));
    }

    /*void composeTelemetry() {

        // At the beginning of each telemetry update, grab a bunch of data
        // from the IMU that we will then display in separate lines.
        telemetry.addAction(new Runnable() { @Override public void run()
        {
            // Acquiring the angles is relatively expensive; we don't want
            // to do that in each of the three items that need that info, as that's
            // three times the necessary expense.
            Orientation angles = imu.getZYXAngles();
            Acceleration gravity  = imu.getGravity();
        }
        });

        telemetry.addLine()
                .addData("status", new Func<String>() {
                    @Override public String value() {
                        return imu.getSystemStatus().toShortString();
                    }
                })
                .addData("calib", new Func<String>() {
                    @Override public String value() {
                        return imu.getCalibrationStatus().toString();
                    }
                });

        telemetry.addLine()
                .addData("heading", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.firstAngle);
                    }
                })
                .addData("roll", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.secondAngle);
                    }
                })
                .addData("pitch", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(angles.angleUnit, angles.thirdAngle);
                    }
                });

        telemetry.addLine()
                .addData("grvty", new Func<String>() {
                    @Override public String value() {
                        return gravity.toString();
                    }
                })
                .addData("mag", new Func<String>() {
                    @Override public String value() {
                        return String.format(Locale.getDefault(), "%.3f",
                                Math.sqrt(gravity.xAccel*gravity.xAccel
                                        + gravity.yAccel*gravity.yAccel
                                        + gravity.zAccel*gravity.zAccel));
                    }
                });
    }
    */
}

package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp(name = "ServoTest")
public class CheckServoPosition extends OpMode {
    Servo wings;
    public void init(){
        wings = hardwareMap.servo.get("wings");
    }
    public void loop(){
        telemetry.addData("wings position", wings.getPosition());
        telemetry.update();
    }
    public void stop(){

    }
}

package org.firstinspires.ftc.teamcode.HackHers1718;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Iris on 12/6/2017.
 */
//assumes one color sensor on the right of the robot when in the right (blue) corner
@Autonomous
@Disabled
public class RedCornerAuto extends LinearOpMode{
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    DcMotor verticalslide;
    Servo jewelservo;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;
    Servo clawservo1;
    Servo clawservo2;

    public void runOpMode()throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        jewelservo = hardwareMap.servo.get("jewelservo");
        clawservo1 = hardwareMap.servo.get("rightclawservo");
        clawservo2 = hardwareMap.servo.get("leftclawservo");
        verticalslide = hardwareMap.dcMotor.get("verticalslide");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);
        verticalslide.setDirection(DcMotorSimple.Direction.REVERSE);

        verticalslide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // get a reference to the color sensor.
        sensorColor = hardwareMap.get(ColorSensor.class, "sensor_color_distance");

        // get a reference to the distance sensor that shares the same name.
        sensorDistance = hardwareMap.get(DistanceSensor.class, "sensor_color_distance");

        // hsvValues is an array that will hold the hue, saturation, and value information.
        float hsvValues[] = {0F, 0F, 0F};

        int[] rgbValues = new int[3];

        // values is a reference to the hsvValues array.
        final float values[] = hsvValues;

        // sometimes it helps to multiply the raw RGB values with a scale factor
        // to amplify/attentuate the measured values.
        final double SCALE_FACTOR = 255;

        // convert the RGB values to HSV values, multiply by the SCALE_FACTOR, then cast it back to int (SCALE_FACTOR is a double)
        Color.RGBToHSV((int) (sensorColor.red() * SCALE_FACTOR),
                (int) (sensorColor.green() * SCALE_FACTOR),
                (int) (sensorColor.blue() * SCALE_FACTOR),
                hsvValues);

        waitForStart();
        if(opModeIsActive()) {
            clawservo1.setPosition(.4);
            clawservo2.setPosition(.7);
            verticalslide.setPower(-.1);
            rightfront.setPower(-.4);
            leftfront.setPower(.4);
            leftback.setPower(-.4);
            rightback.setPower(.4);
            sleep(150);
            drive(0);
            jewelservo.setPosition(.15);
            sleep(1000);

            rgbValues[0] = sensorColor.red();
            rgbValues[1] = sensorColor.green();
            rgbValues[2] = sensorColor.blue();

            //if colorsensor sees red, drive backward, then lift servo and drive forward. Translate left and then sleep
            if (rgbValues[0] > 2.5 * rgbValues[2] /*&& hsvValues[0] > 300*/) {
                telemetry.addLine("Red");
                telemetry.update();
                sleep(1000);

                drive(.2);
                sleep(600);
                drive(0);
                sleep(200);
                jewelservo.setPosition(.9);
                sleep(500);
                drive(.4);
                sleep(1400);
                drive(0);
                sleep(200);
                rightfront.setPower(.5);
                leftfront.setPower(-.5);
                leftback.setPower(.5);
                rightback.setPower(-.5);
                sleep(1400);
                drive(0);

                rightfront.setPower(-.5);
                leftfront.setPower(.5);
                rightback.setPower(-.5);
                leftback.setPower(.5);
                sleep(1800);

                drive(-.5);
                sleep(500);
                drive(0);

                clawservo1.setPosition(.8);
                clawservo2.setPosition(.25);
                drive(.2);
                sleep(400);
                drive(0);
                //if colorsensor sees blue, drive forward, lift servo, and drive forward slightly. Translate left then sleep.
            } else if (rgbValues[2] > 2.2 * rgbValues[0] /*&& hsvValues[0] > 200*/) {
                telemetry.addLine("Blue");
                telemetry.update();
                sleep(1000);

                drive(-.2);
                sleep(400);
                drive(0);
                sleep(200);
                jewelservo.setPosition(.9);
                sleep(500);
                drive(.4);
                sleep(1337);
                drive(0);
                sleep(200);
                rightfront.setPower(.5);
                leftfront.setPower(-.5);
                leftback.setPower(.5);
                rightback.setPower(-.5);
                sleep(1200);
                drive(0);

                rightfront.setPower(-.5);
                leftfront.setPower(.5);
                rightback.setPower(-.5);
                leftback.setPower(.5);
                sleep(1800);

                drive(-.5);
                sleep(500);
                drive(0);
                clawservo1.setPosition(.8);
                clawservo2.setPosition(.25);
                drive(.2);
                sleep(200);
                drive(0);
            } else{
                jewelservo.setPosition(.9);
                sleep(500);
                drive(.4);
                sleep(1337);
                drive(0);
                sleep(200);
                rightfront.setPower(.5);
                leftfront.setPower(-.5);
                leftback.setPower(.5);
                rightback.setPower(-.5);
                sleep(1200);
                drive(0);

                rightfront.setPower(-.5);
                leftfront.setPower(.5);
                rightback.setPower(-.5);
                leftback.setPower(.5);
                sleep(1800);

                drive(-.5);
                sleep(500);
                drive(0);
                clawservo1.setPosition(.8);
                clawservo2.setPosition(.25);
                drive(.2);
                sleep(200);
                drive(0);
            }
        }
    }
    //Purely to condense code. Sets all motors to one speed. Can be used to drive forward, backward, or stop.
    public void drive(double speed) {
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

}

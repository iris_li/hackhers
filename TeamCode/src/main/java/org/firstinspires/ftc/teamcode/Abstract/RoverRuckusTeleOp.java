package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;

@TeleOp(name = "RoverRuckusTeleOp")
public class RoverRuckusTeleOp extends OpMode/*extends RoverRuckusHardware*/{
    RobotHardware robot;
    public void init(){
        robot = new RobotHardware(hardwareMap);
    }
    public void loop(){
        // left stick controls direction
        // right stick X controls rotation
        double gamepad1LeftY = gamepad1.left_stick_y;
        double gamepad1LeftX = -gamepad1.left_stick_x;
        double gamepad1RightX = -gamepad1.right_stick_x;

        double FrontLeft = gamepad1LeftY + gamepad1LeftX + gamepad1RightX;
        double FrontRight = gamepad1LeftY - gamepad1LeftX - gamepad1RightX;
        double BackRight = gamepad1LeftY + gamepad1LeftX - gamepad1RightX;
        double BackLeft = gamepad1LeftY - gamepad1LeftX + gamepad1RightX;

        // clip the right/left values so that the values never exceed +/- 1
        FrontRight = Range.clip(FrontRight, -1, 1);
        FrontLeft = Range.clip(FrontLeft, -1, 1);
        BackLeft = Range.clip(BackLeft, -1, 1);
        BackRight = Range.clip(BackRight, -1, 1);

        // set power to motors
        robot.setMotorPowers(scaleInput(FrontRight), scaleInput(BackRight),
                scaleInput(FrontLeft),scaleInput(BackLeft));

        if (gamepad2.dpad_down) {
            robot.setDropperPos(.4);
        } else {
            robot.setDropperPos(.9);
        }
        //lift
        if (gamepad2.a){
            robot.setLiftPower(.3);
        } else if(gamepad2.b){
            robot.setLiftPower(-.3);
        } else {
            robot.setLiftPower(.1);
        }
    }
    @Override
    public void stop(){
    }

    double scaleInput(double dVal)  {
        double[] scaleArray = { 0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
                0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00 };

        // get the corresponding index for the scaleInput array.
        int index = (int) (dVal * 16.0);

        // index should be positive.
        if (index < 0) {
            index = -index;
        }

        // index cannot exceed size of array minus 1.
        if (index > 16) {
            index = 16;
        }

        // get value from the array.
        double dScale = 0.0;
        if (dVal < 0) {
            dScale = -scaleArray[index];
        } else {
            dScale = scaleArray[index];
        }

        // return scaled value.
        return dScale;
    }
}

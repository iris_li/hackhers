package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;


@Autonomous (name="StrafeTest")
@Disabled
public class StrafeTest extends LinearOpMode {
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    Servo bjoint;
    Servo mjoint;
    CRServo intake;
    CRServo tjoint;

    public void runOpMode() throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");

        // have not used servos yet, will use them for arm later
        //bjoint = hardwareMap.servo.get("bottom joint");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        //sets motors to stop moving when they don't receive any inputs, prevents drifting
        rightfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        waitForStart();
        if (opModeIsActive()) {
            strafeLeft(.5,1000);
            strafeRight(.5,1000);
        }
    }
    public void strafeRight(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(power);
        leftfront.setPower(power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeLeft(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(-power);
        leftfront.setPower(-power);
        leftback.setPower(power);
        sleep(time);
    }
}

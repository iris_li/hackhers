package org.firstinspires.ftc.teamcode.HackHers1718;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by Iris on 2/27/2018.
 */
@Autonomous (name="VuforiaBlueCorner")
@Disabled
public class VuforiaBlueCorner extends LinearOpMode{
    final int encRotation = 1120;
    VuforiaLocalizer vuforia;

    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    DcMotor verticalslide;
    Servo jewelservo;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;
    Servo clawservo1;
    Servo clawservo2;

    public void runOpMode()throws InterruptedException {

        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        parameters.vuforiaLicenseKey = "ATdNmSj/////AAAAGbyDWOnrRElaliMqraHH4KhjvGb/OKhjgEmC+49FLa7/84Vz5KtVXOzTAgSqea+Vmd2yiZw+xqy78+grFeeSlhYA/1lPRgNQEwrfT4h8AFUKAbi39OGAHa0bE/UeO1IEM7om0fu33E/4N8u2oVht0HKZCr38W3pSbc2L9S1wBZJ/IPc/xnd7EiNLlphOtb307VjDxTzUWMLWguHbRYZVNWbAD8Nssbsi02QNtUBDpRsXSbBqNlWRYhweoqNbWoucWVr6st4Z3fRbF3O67NT4vqpQTuuFzqG58fDH/Rv3U4x9VwbvVTk0VxUY3veB3UrRtfExH31jQnUoEZny0XccVLXZEx8sU2WBrdiZu3r5S3Ct";
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);

        VuforiaTrackables relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate"); // can help in debugging; otherwise not necessary

        telemetry.addData(">", "Press Play to start");
        telemetry.update();

        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        jewelservo = hardwareMap.servo.get("jewelservo");
        clawservo1 = hardwareMap.servo.get("rightclawservo");
        clawservo2 = hardwareMap.servo.get("leftclawservo");
        verticalslide = hardwareMap.dcMotor.get("verticalslide");

        leftfront.setDirection(DcMotorSimple.Direction.REVERSE);
        leftback.setDirection(DcMotorSimple.Direction.REVERSE);
        verticalslide.setDirection(DcMotorSimple.Direction.REVERSE);

        verticalslide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        this.rightfront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.leftfront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.rightback.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.leftback.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftfront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightfront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftback.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightback.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // get a reference to the color sensor.
        sensorColor = hardwareMap.get(ColorSensor.class, "sensor_color_distance");

        // get a reference to the distance sensor that shares the same name.
        sensorDistance = hardwareMap.get(DistanceSensor.class, "sensor_color_distance");

        // hsvValues is an array that will hold the hue, saturation, and value information.
        float hsvValues[] = {0F, 0F, 0F};

        int[] rgbValues = new int[3];
        rgbValues[0] = sensorColor.red();
        rgbValues[1] = sensorColor.green();
        rgbValues[2] = sensorColor.blue();

        // values is a reference to the hsvValues array.
        final float values[] = hsvValues;

        // sometimes it helps to multiply the raw RGB values with a scale factor
        // to amplify/attentuate the measured values.
        final double SCALE_FACTOR = 255;

        // convert the RGB values to HSV values, multiply by the SCALE_FACTOR, then cast it back to int (SCALE_FACTOR is a double)
        Color.RGBToHSV((int) (sensorColor.red() * SCALE_FACTOR),
                (int) (sensorColor.green() * SCALE_FACTOR),
                (int) (sensorColor.blue() * SCALE_FACTOR),
                hsvValues);

        waitForStart();
        relicTrackables.activate();

        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.from(relicTemplate);

        if (opModeIsActive()) {
            clawservo1.setPosition(.4);
            clawservo2.setPosition(.7);
            verticalslide.setPower(-.1);
            strafeLeft(.2, .4);
            jewelservo.setPosition(.15);
            sleep(1000);

            rgbValues[0] = sensorColor.red();
            rgbValues[1] = sensorColor.green();
            rgbValues[2] = sensorColor.blue();

            //if colorsensor sees red, drive forward, then lift servo and drive backwards. Translate left and then sleep.
            if (rgbValues[0] > 2.2 * rgbValues[2]) {
                telemetry.addLine("Red");
                telemetry.update();
                sleep(1000);

                driveWithEncoders(.3, .4);
                jewelservo.setPosition(.9);
                sleep(500);
                strafeRight(.4, .4);
                sleep(1000);

                if(vuMark != RelicRecoveryVuMark.UNKNOWN) {
                    telemetry.addData("VuMark", "%s visible", vuMark);
                    telemetry.update();
                    sleep(1000);

                    if (vuMark == RelicRecoveryVuMark.LEFT) {
                        driveWithEncoders(2, .4);
                        drive(0);
                        strafeRight(.5, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    } else if (vuMark == RelicRecoveryVuMark.CENTER) {
                        driveWithEncoders(2, .4);
                        drive(0);
                        strafeRight(.8, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    } else if (vuMark == RelicRecoveryVuMark.RIGHT) {
                        driveWithEncoders(2, .4);
                        drive(0);
                        strafeRight(1, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    }
                    clawservo1.setPosition(.8);
                    clawservo2.setPosition(.25);
                    driveBack(.5, -.4);
                    drive(0);

                } else {
                    telemetry.addData("VuMark", "not visible");
                    telemetry.update();
                }

                //if colorsensor sees blue, drive backward, lift servo, and drive backwards. Translate left then sleep.
            } else if (rgbValues[2] > 2.5 * rgbValues[0]) {
                telemetry.addLine("Blue");
                telemetry.update();
                sleep(1000);
                driveBack(.3, -.4);
                drive(0);
                jewelservo.setPosition(.9);
                sleep(500);
                strafeRight(.4, .4);
                sleep(1000);

                if(vuMark != RelicRecoveryVuMark.UNKNOWN) {
                    telemetry.addData("VuMark", "%s visible", vuMark);
                    telemetry.update();
                    sleep(1000);

                    if (vuMark == RelicRecoveryVuMark.LEFT) {
                        driveWithEncoders(2.5, .4);
                        drive(0);
                        strafeRight(.5, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    } else if (vuMark == RelicRecoveryVuMark.CENTER) {
                        driveWithEncoders(2.5, .4);
                        drive(0);
                        strafeRight(.8, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    } else if (vuMark == RelicRecoveryVuMark.RIGHT) {
                        driveWithEncoders(2.5, .4);
                        drive(0);
                        strafeRight(1, .4);
                        drive(0);
                        driveWithEncoders(.5, .4);
                        drive(0);
                    } else {
                        telemetry.addData("VuMark", "not visible");
                        telemetry.update();
                    }
                    clawservo1.setPosition(.8);
                    clawservo2.setPosition(.25);
                    driveBack(.3, .4);
                    drive(0);
                }
            } else {
                jewelservo.setPosition(.9);
                sleep(500);
                driveWithEncoders(2.5, .4);
                drive(0);
                strafeRight(.5, .4);
                drive(0);
                driveWithEncoders(.5, .4);
                drive(0);
                clawservo1.setPosition(.8);
                clawservo2.setPosition(.25);
                driveBack(.5, -.4);
                drive(0);
            }
        }
    }

    /** Drive (forward) the indicated number of motor shaft revolutions using the indicated power */
    public void driveWithEncoders(double revolutions, double power) throws InterruptedException {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int)Math.round(revolutions * encRotation);
        if (opModeIsActive()){

            // Tell the motors where we are going
            this.leftfront.setTargetPosition(this.leftfront.getCurrentPosition() + denc);
            this.rightfront.setTargetPosition(this.rightfront.getCurrentPosition() + denc);
            this.leftback.setTargetPosition(this.leftback.getCurrentPosition() + denc);
            this.rightback.setTargetPosition(this.rightback.getCurrentPosition() + denc);

            // Set them a-going
            this.leftfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.leftback.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightback.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.leftfront.setPower(power);
            this.rightfront.setPower(power);
            this.leftback.setPower(power);
            this.rightback.setPower(power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.leftfront.isBusy() || this.rightfront.isBusy() || this.leftback.isBusy() || this.rightback.isBusy())) {
                telemetry.addData("LFEncoder", leftfront.getCurrentPosition());
                telemetry.addData("RFEncoder", rightfront.getCurrentPosition());
                telemetry.addData("LBEncoder", leftback.getCurrentPosition());
                telemetry.addData("RBEncoder", rightback.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }

    String format(OpenGLMatrix transformationMatrix) {
        return (transformationMatrix != null) ? transformationMatrix.formatAsTransform() : "null";
    }
    //Purely to condense code. Sets all motors to one speed. Can be used to drive forward, backward, or stop.
    public void drive(double speed){
        leftfront.setPower(speed);
        rightfront.setPower(speed);
        leftback.setPower(speed);
        rightback.setPower(speed);

        leftfront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightfront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftback.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightback.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }
    public void turnLeft(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(-power);
        leftfront.setPower(power);
        leftback.setPower(power);
        sleep(time);
    }
    public void turnRight(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(power);
        leftfront.setPower(-power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeRight(double revolutions, double power){
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int)Math.round(revolutions * encRotation);
        if (opModeIsActive()){

            // Tell the motors where we are going
            this.leftfront.setTargetPosition(this.leftfront.getCurrentPosition() + denc);
            this.rightfront.setTargetPosition(this.rightfront.getCurrentPosition() - denc);
            this.leftback.setTargetPosition(this.leftback.getCurrentPosition() - denc);
            this.rightback.setTargetPosition(this.rightback.getCurrentPosition() + denc);

            // Set them a-going
            this.leftfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.leftback.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightback.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.leftfront.setPower(power);
            this.rightfront.setPower(-power);
            this.leftback.setPower(-power);
            this.rightback.setPower(power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.leftfront.isBusy() || this.rightfront.isBusy() || this.leftback.isBusy() || this.rightback.isBusy())) {
                telemetry.addData("LFEncoder", leftfront.getCurrentPosition());
                telemetry.addData("RFEncoder", rightfront.getCurrentPosition());
                telemetry.addData("LBEncoder", leftback.getCurrentPosition());
                telemetry.addData("RBEncoder", rightback.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }
    public void strafeLeft(double revolutions, double power) {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int) Math.round(revolutions * encRotation);
        if (opModeIsActive()) {

            // Tell the motors where we are going
            this.leftfront.setTargetPosition(this.leftfront.getCurrentPosition() - denc);
            this.rightfront.setTargetPosition(this.rightfront.getCurrentPosition() + denc);
            this.leftback.setTargetPosition(this.leftback.getCurrentPosition() + denc);
            this.rightback.setTargetPosition(this.rightback.getCurrentPosition() - denc);

            // Set them a-going
            this.leftfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.leftback.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightback.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.leftfront.setPower(-power);
            this.rightfront.setPower(power);
            this.leftback.setPower(power);
            this.rightback.setPower(-power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.leftfront.isBusy() || this.rightfront.isBusy() || this.leftback.isBusy() || this.rightback.isBusy())) {
                telemetry.addData("LFEncoder", leftfront.getCurrentPosition());
                telemetry.addData("RFEncoder", rightfront.getCurrentPosition());
                telemetry.addData("LBEncoder", leftback.getCurrentPosition());
                telemetry.addData("RBEncoder", rightback.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }
    public void driveBack(double revolutions, double power) {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int) Math.round(revolutions * encRotation);
        if (opModeIsActive()) {

            // Tell the motors where we are going
            this.leftfront.setTargetPosition(this.leftfront.getCurrentPosition() - denc);
            this.rightfront.setTargetPosition(this.rightfront.getCurrentPosition() - denc);
            this.leftback.setTargetPosition(this.leftback.getCurrentPosition() - denc);
            this.rightback.setTargetPosition(this.rightback.getCurrentPosition() - denc);

            // Set them a-going
            this.leftfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightfront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.leftback.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.rightback.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.leftfront.setPower(power);
            this.rightfront.setPower(power);
            this.leftback.setPower(power);
            this.rightback.setPower(power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.leftfront.isBusy() || this.rightfront.isBusy() || this.leftback.isBusy() || this.rightback.isBusy())) {
                telemetry.addData("LFEncoder", leftfront.getCurrentPosition());
                telemetry.addData("RFEncoder", rightfront.getCurrentPosition());
                telemetry.addData("LBEncoder", leftback.getCurrentPosition());
                telemetry.addData("RBEncoder", rightback.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }
}


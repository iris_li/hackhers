package org.firstinspires.ftc.teamcode.HackHers1718;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Iris, Anna, Phoebe, and Sophia on 10/19/2017.
 */
// red is bad >:(
@TeleOp (name= "TankDrive")
@Disabled
    public class TankDrive extends LinearOpMode{
//Teleop tank drive without scaling (will probably not use)
    //declare motors
        DcMotor leftfront;
        DcMotor leftback;
        DcMotor rightfront;
        DcMotor rightback;
        Servo jewelservo;
//huhuhu
        public void runOpMode() throws InterruptedException{
            leftfront = hardwareMap.dcMotor.get("leftfront");
            leftback = hardwareMap.dcMotor.get("leftback");
            rightfront = hardwareMap.dcMotor.get("rightfront");
            rightback = hardwareMap.dcMotor.get("rightback");
            jewelservo = hardwareMap.servo.get("jewelservo");

            double threshold = 0.2;
            double sidewayspeed = 1;

            //Reverses right motor direction because they face the opposite direction
            rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
            rightback.setDirection(DcMotorSimple.Direction.REVERSE);
            //sets servo default position

            waitForStart();
            while(opModeIsActive()){
                double rightjoystick1 = gamepad1.right_stick_y;
                double leftjoystick1 = gamepad1.left_stick_y;
                boolean rightbumper1 = gamepad1.right_bumper;
                boolean leftbumper1 = gamepad1.left_bumper;

                if (gamepad2.a){
                    jewelservo.setPosition(.3);
                }else {
                    jewelservo.setPosition(.9);
                }

                //tank drive
                //if right joystick is forward, right motors go forward
                //if right joystick is backward, right motors go backward
                if (rightjoystick1 >= threshold) {
                    rightfront.setPower(.5);
                    rightback.setPower(.5);
                } else if (rightjoystick1 <= -threshold) {
                    rightfront.setPower(-.5);
                    rightback.setPower(-.5);
                } else if (!rightbumper1 || !leftbumper1) {
                    rightfront.setPower(0);
                    rightback.setPower(0);
                }
                //if left joystick is forward, left motors go forward
                //if left joystick is backward, left motors go backward
                if (leftjoystick1 >= threshold) {
                    leftfront.setPower(.5);
                    leftback.setPower(.5);
                } else if (leftjoystick1 <= -threshold) {
                    leftfront.setPower(-.5);
                    leftback.setPower(-.5);
                } else if (!rightbumper1 || !leftbumper1) {
                    leftfront.setPower(0);
                    leftback.setPower(0);
                }
                //sideways drive (for mecanum wheels)
                //first if prevents robot from being controlled with joysticks and bumpers at the same time
                if (rightjoystick1 != threshold && rightjoystick1 != -threshold && leftjoystick1 != threshold && leftjoystick1 != -threshold) {
                    if (rightbumper1) {
                        rightfront.setPower(-.5);
                        rightback.setPower(.5);
                        leftfront.setPower(.5);
                        leftback.setPower(-.5);
                    } else if (leftbumper1) {
                        rightfront.setPower(.5);
                        rightback.setPower(-.5);
                        leftfront.setPower(-.5);
                        leftback.setPower(.5);
                    } else {
                        rightfront.setPower(0);
                        leftback.setPower(0);
                        rightback.setPower(0);
                        leftfront.setPower(0);
                    }
                }
            }
        }
    }

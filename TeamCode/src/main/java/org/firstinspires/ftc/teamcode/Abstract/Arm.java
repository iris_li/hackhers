package org.firstinspires.ftc.teamcode.Abstract;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

public class Arm{
    HardwareMap hardware;
    public void init(HardwareMap hardware){
        //bottom, middle, and top joint

        Servo bjoint;
        Servo mjoint;
        CRServo tjoint;
        CRServo intake;
        bjoint = hardware.servo.get("bjoint");
        mjoint = hardware.servo.get("mjoint");
        tjoint = hardware.crservo.get("tjoint");
        intake = hardware.crservo.get("intake");
        pivot(0, bjoint);
        pivot(.7,mjoint);
        spin(0,tjoint);
        spin(0,intake);
    }

    public void pivot(double position, Servo servo){
        while(servo.getPosition() < position) {
            servo.setPosition(servo.getPosition() + (position*.1));
        }
        while(servo.getPosition() > position){
            servo.setPosition(servo.getPosition() - (position*.1));
        }
    }
    public void pivot(boolean posdir, Servo servo){
        if(posdir){
            servo.setPosition(servo.getPosition() + .1);
        }
        else if (!posdir){
            servo.setPosition(servo.getPosition() - .1);
        }
    }
    public void spin(double power, CRServo servo){
            servo.setPower(power);
    }
}

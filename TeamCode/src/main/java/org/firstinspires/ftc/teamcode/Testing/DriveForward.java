package org.firstinspires.ftc.teamcode.Testing;

import android.graphics.Path;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

/**
 * Created by Iris on 11/30/2017.
 */
@Autonomous (name="DriveForward")

public class DriveForward extends LinearOpMode{
    DcMotor motorFrontRight;
    DcMotor motorFrontLeft;
    DcMotor motorBackRight;
    DcMotor motorBackLeft;
    public void runOpMode()throws InterruptedException{
        motorFrontRight = hardwareMap.dcMotor.get("rightfront");
        motorFrontLeft = hardwareMap.dcMotor.get("leftfront");
        motorBackLeft = hardwareMap.dcMotor.get("leftback");
        motorBackRight = hardwareMap.dcMotor.get("rightback");

        motorFrontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        motorBackLeft.setDirection(DcMotorSimple.Direction.REVERSE);

        motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        motorBackRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        motorBackLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);


//apparently the forward is backward from the teleop opmode
        waitForStart();
        while(opModeIsActive()) {
            telemetry.addData("LeftFront", motorFrontLeft.getCurrentPosition());
            telemetry.addData("RightBack", motorBackRight.getCurrentPosition());
            telemetry.addData("LeftBack", motorBackLeft.getCurrentPosition());
            telemetry.addData("RightFront",motorFrontRight.getCurrentPosition());

            telemetry.update();
        }
    }
}

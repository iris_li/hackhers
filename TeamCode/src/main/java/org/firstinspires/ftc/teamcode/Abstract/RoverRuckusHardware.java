package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import java.util.Locale;

//is having errors when used by RoverRuckusTeleop
public abstract class RoverRuckusHardware extends OpMode {
    DcMotor rf, lf, rb, lb, lift;
    Servo dropper;
    IMU imu;

    //when upright, heading = 0, roll =-90, pitch = 0

    //hardwaremapping
    public void init() {
        rf = hardwareMap.dcMotor.get("rightfront");
        lf = hardwareMap.dcMotor.get("leftfront");
        rb = hardwareMap.dcMotor.get("rightback");
        lb = hardwareMap.dcMotor.get("leftback");
        lift = hardwareMap.dcMotor.get("lift");
        dropper = hardwareMap.servo.get("dropper");
        imu = new IMU(hardwareMap);

        rf.setDirection(DcMotorSimple.Direction.REVERSE);
        rb.setDirection(DcMotorSimple.Direction.REVERSE);

        rf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    public void autoTelemetry(){
        telemetry.addData("Heading(Z)", imu.getAngleZ());
        telemetry.addData("Roll(Y)", imu.getAngleY());
        telemetry.addData("Pitch(X)", imu.getAngleX());
        telemetry.addData("Gravity", imu.getGravity());
        telemetry.update();
    }
}

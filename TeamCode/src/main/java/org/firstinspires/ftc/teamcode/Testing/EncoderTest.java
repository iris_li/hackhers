package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.*;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Func;

//one revolution = approx 9 inches
/**
 * This simple OpMode illustrates how to drive autonomously a certain distance using encoders.
 *
 * The OpMode works with both legacy and modern motor controllers. It expects two motors,
 * named "motorLeft" and "motorRight".
 */
@Autonomous(name="EncoderTest")
public class EncoderTest extends LinearOpMode{
    //----------------------------------------------------------------------------------------------
    // State
    //----------------------------------------------------------------------------------------------

    // The number of encoder ticks per motor shaft revolution. 1120 is correct
    final int encRotation = 1120;

    DcMotor motorRightFront;
    DcMotor motorLeftFront;
    DcMotor motorRightBack;
    DcMotor motorLeftBack;

    //----------------------------------------------------------------------------------------------
    // Main loop
    //----------------------------------------------------------------------------------------------

    public void runOpMode() throws InterruptedException {
    //    this.composeDashboard();
        this.motorRightFront = hardwareMap.dcMotor.get("rightfront");
        this.motorLeftFront = hardwareMap.dcMotor.get("leftfront");
        this.motorRightBack = hardwareMap.dcMotor.get("rightback");
        this.motorLeftBack = hardwareMap.dcMotor.get("leftback");

        motorLeftFront.setDirection(DcMotor.Direction.REVERSE);
        motorLeftBack.setDirection(DcMotor.Direction.REVERSE);

        this.motorRightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.motorLeftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.motorRightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        this.motorLeftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        waitForStart();

        // Reset the encoders to zero.
        //
        // Note: we can do this, or not do this. The rest of the code is based only on *increments*
        // to the positions, so we could live with whatever the encoders happen to presently read
        // just fine. That said, it's a little easier to interpret telemetry if we start them off
        // at zero, so we do that. But try commenting these lines out, and observe that the code
        // continues to work just fine, even as you run the OpMode multiple times.

        // Drive forward a while. The parameters here are arbitrary; they're just for illustration
        driveWithEncoders(1, .1);
    }

    /** Drive (forward) the indicated number of motor shaft revolutions using the indicated power */
    public void driveWithEncoders(double revolutions, double power) throws InterruptedException {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int)Math.round(revolutions * encRotation);
        if (opModeIsActive()){

            // Tell the motors where we are going
            this.motorLeftFront.setTargetPosition(this.motorLeftFront.getCurrentPosition() + denc);
            this.motorRightFront.setTargetPosition(this.motorRightFront.getCurrentPosition() + denc);
            this.motorLeftBack.setTargetPosition(this.motorLeftBack.getCurrentPosition() + denc);
            this.motorRightBack.setTargetPosition(this.motorRightBack.getCurrentPosition() + denc);

            // Set them a-going
            this.motorLeftFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorLeftBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.motorLeftFront.setPower(power);
            this.motorRightFront.setPower(power);
            this.motorLeftBack.setPower(power);
            this.motorRightBack.setPower(power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.motorLeftFront.isBusy() || this.motorRightFront.isBusy() || this.motorLeftBack.isBusy() || this.motorRightBack.isBusy())) {
                telemetry.addData("LFEncoder", motorLeftFront.getCurrentPosition());
                telemetry.addData("RFEncoder", motorRightFront.getCurrentPosition());
                telemetry.addData("LBEncoder", motorLeftBack.getCurrentPosition());
                telemetry.addData("RBEncoder", motorRightBack.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }

    public void strafeRight(double revolutions, double power) throws InterruptedException {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int)Math.round(revolutions * encRotation);
        if (opModeIsActive()){

            // Tell the motors where we are going
            this.motorLeftFront.setTargetPosition(this.motorLeftFront.getCurrentPosition() + denc);
            this.motorRightFront.setTargetPosition(this.motorRightFront.getCurrentPosition() + denc);
            this.motorLeftBack.setTargetPosition(this.motorLeftBack.getCurrentPosition() + denc);
            this.motorRightBack.setTargetPosition(this.motorRightBack.getCurrentPosition() + denc);

            // Set them a-going
            this.motorLeftFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorLeftBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.motorLeftFront.setPower(-power);
            this.motorRightFront.setPower(power);
            this.motorLeftBack.setPower(power);
            this.motorRightBack.setPower(-power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.motorLeftFront.isBusy() || this.motorRightFront.isBusy() || this.motorLeftBack.isBusy() || this.motorRightBack.isBusy())) {
                telemetry.addData("LFEncoder", motorLeftFront.getCurrentPosition());
                telemetry.addData("RFEncoder", motorRightFront.getCurrentPosition());
                telemetry.addData("LBEncoder", motorLeftBack.getCurrentPosition());
                telemetry.addData("RBEncoder", motorRightBack.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }

    public void strafeLeft(double revolutions, double power) throws InterruptedException {
        // How far are we to move, in ticks instead of revolutions?
        int denc = (int)Math.round(revolutions * encRotation);
        if (opModeIsActive()){

            // Tell the motors where we are going
            this.motorLeftFront.setTargetPosition(this.motorLeftFront.getCurrentPosition() + denc);
            this.motorRightFront.setTargetPosition(this.motorRightFront.getCurrentPosition() + denc);
            this.motorLeftBack.setTargetPosition(this.motorLeftBack.getCurrentPosition() + denc);
            this.motorRightBack.setTargetPosition(this.motorRightBack.getCurrentPosition() + denc);

            // Set them a-going
            this.motorLeftFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightFront.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorLeftBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.motorRightBack.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // Give them the power level we want them to move at
            this.motorLeftFront.setPower(power);
            this.motorRightFront.setPower(-power);
            this.motorLeftBack.setPower(-power);
            this.motorRightBack.setPower(power);

            power = Range.clip(Math.abs(power), -1.0, 1.0);

            // Wait until they are done
            while (opModeIsActive() && (this.motorLeftFront.isBusy() || this.motorRightFront.isBusy() || this.motorLeftBack.isBusy() || this.motorRightBack.isBusy())) {
                telemetry.addData("LFEncoder", motorLeftFront.getCurrentPosition());
                telemetry.addData("RFEncoder", motorRightFront.getCurrentPosition());
                telemetry.addData("LBEncoder", motorLeftBack.getCurrentPosition());
                telemetry.addData("RBEncoder", motorRightBack.getCurrentPosition());
                telemetry.update();
            }
            drive(0);
            sleep(500);
            // Always leave the screen looking pretty
            telemetry.update();
        }
    }

    //----------------------------------------------------------------------------------------------
    // Utility
    //----------------------------------------------------------------------------------------------
/*
    void composeDashboard()
    {
        telemetry.addLine()
                .addData("left: ",     new Func<Object>() { public Object value() { return motorLeftFront.getCurrentPosition(); }})
                .addData("target: ",   new Func<Object>() { public Object value() { return motorLeftFront.getTargetPosition(); }})
                .addData("mode: ",     new Func<Object>() { public Object value() { return format(motorLeftFront.getMode()); }});


        telemetry.addLine()
                .addData("right: ", new IFunc<Object>() { public Object value() { return motorRightFront.getCurrentPosition(); }})
                .addData("target: ",    new IFunc<Object>() { public Object value() { return motorRightFront.getTargetPosition(); }})
                .addData("mode: ",      new IFunc<Object>() { public Object value() { return format(motorRightFront.getMode()); }});

        telemetry.addLine()
                .addData("left: ",     new Func<Object>() { public Object value() { return motorLeftBack.getCurrentPosition(); }})
                .addData("target: ",   new Func<Object>() { public Object value() { return motorLeftBack.getTargetPosition(); }})
                .addData("mode: ",     new Func<Object>() { public Object value() { return format(motorLeftBack.getMode()); }});


        telemetry.addLine()
                .addData("right: ", new IFunc<Object>() { public Object value() { return motorRightBack.getCurrentPosition(); }})
                .addData("target: ",    new IFunc<Object>() { public Object value() { return motorRightBack.getTargetPosition(); }})
                .addData("mode: ",      new IFunc<Object>() { public Object value() { return format(motorRightBack.getMode()); }});

    }
*/

    public interface IFunc<TResult>
    {
        /**
         * Evaluates and returns the value of the function
         * @return  the value of the function
         */
        TResult value();
    }

    public void drive(double speed){
        motorLeftFront.setPower(-speed);
        motorRightFront.setPower(speed);
        motorLeftBack.setPower(speed);
        motorRightBack.setPower(-speed);

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    String format(DcMotor.RunMode mode)
    {
        switch (mode)
        {
            default:
            case RUN_WITHOUT_ENCODER:       return "run";
            case STOP_AND_RESET_ENCODER:    return "reset";
            case RUN_TO_POSITION:           return "runToPos";
            case RUN_USING_ENCODER:         return "runEnc";
        }
    }
}
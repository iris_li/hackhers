package org.firstinspires.ftc.teamcode; /**
 * Created by Phoebe on 5/29/18.
 */

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

@TeleOp(name = "Outreachbot")
public class Outreachbot extends OpMode {
    DcMotor right;
    DcMotor left;
    Servo wings;

    public Outreachbot() {
    }

    @Override
    public void init() {
        right = hardwareMap.dcMotor.get("right");
        left = hardwareMap.dcMotor.get("left");
        wings = hardwareMap.servo.get("wings");

        right.setDirection(DcMotorSimple.Direction.REVERSE);

        left.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        right.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    @Override
    public void loop() {
        // left stick controls direction
        // right stick X controls rotation
        //front is neg, back is pos
        float gamepad1LeftY = -gamepad1.left_stick_y;
        float gamepad1RightY = -gamepad1.right_stick_y;

        float power = gamepad1LeftY;
        power = Range.clip(power, -1, 1);

        float BackLeft = power + gamepad1RightY;
        float BackRight = power - gamepad1RightY;

        BackLeft = Range.clip(BackLeft, -1, 1);
        BackRight = Range.clip(BackRight, -1, 1);

        left.setPower(BackLeft);
        right.setPower(BackRight);

        //uses gamepad to control1 wing position
        if(gamepad1.a){
            wings.setPosition(.1);
        }
        //while robot is running, wings open
        else if (BackRight != 0 && BackLeft != 0){
            wings.setPosition(.1);
        }else {
            wings.setPosition(.4);
        }
    }
    public void stop() {
    }
    double scaleInput(double dVal)  {
        double[] scaleArray = { 0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
                0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00 };

        // get the corresponding index for the scaleInput array.
        int index = (int) (dVal * 16.0);

        // index should be positive.
        if (index < 0) {
            index = -index;
        }

        // index cannot exceed size of array minus 1.
        if (index > 16) {
            index = 16;
        }

        // get value from the array.
        double dScale = 0.0;
        if (dVal < 0) {
            dScale = -scaleArray[index];
        } else {
            dScale = scaleArray[index];
        }
        // return scaled value.
        return dScale;
    }
}


package org.firstinspires.ftc.teamcode.HackHers1718;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Iris on 11/9/2017.
 */
@TeleOp (name = "JewelServo")
@Disabled
    public class JewelServo extends LinearOpMode{
        Servo jewelservo;

        public void runOpMode()throws InterruptedException{
            jewelservo = hardwareMap.servo.get("jewelservo");

            waitForStart();
            while(opModeIsActive()){
                boolean button = gamepad2.a;

                if(button){
                    jewelservo.setPosition(.3);
                }else {
                    jewelservo.setPosition(.9);
                }
            }
        }


}

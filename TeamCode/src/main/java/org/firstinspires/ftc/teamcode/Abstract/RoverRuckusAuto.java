package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous (name = "RoverRuckusAuto")
@Disabled
public class RoverRuckusAuto extends RoverRuckusHardware {
    RobotState currentState;
    public void init(){
        currentState = States.INITIALIZING;
    }
    public void loop(){
        autoTelemetry();
        telemetry.addData("State: ", currentState.toString());
        if(currentState != currentState.nextState()){
            currentState = currentState.nextState();
            currentState.init();
            currentState.execute();
        }

    }
    public void stop(){

    }

    interface RobotState {
        void init();
        void execute();
        RobotState nextState();
    }
    enum States implements RobotState{
        INITIALIZING{
            @Override
            public void init(){

            }

            @Override
            public void execute() {

            }

            @Override
            public RobotState nextState() {
                return SAMPLING;
            }
        },
        SAMPLING{
            @Override
            public void init(){

            }

            @Override
            public void execute() {

            }

            @Override
            public RobotState nextState() {
                return CLAIMING;
            }
        },
        CLAIMING{
            @Override
            public void init(){

            }

            @Override
            public void execute() {

            }

            @Override
            public RobotState nextState() {
                return PARKING;
            }
        },
        PARKING{
            @Override
            public void init(){

            }

            @Override
            public void execute() {

            }

            @Override
            public RobotState nextState() {
                return null;
            }
        }
    }
}

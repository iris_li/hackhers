package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * Created by Iris, Shih Ting, and Phoebe on 11/16/17.
 */
@Autonomous (name="NullOpMode")
@Disabled
public class NullOpMode extends OpMode {
     public void init() {
         telemetry.addLine("failed");
     }
     public void loop(){
        telemetry.addLine("OpMode is Active");
     }
     public void stop(){
     }
}

package org.firstinspires.ftc.teamcode.HackHers1718;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by HackHers on 4/5/2018.
 */

public class Dance extends LinearOpMode {
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    DcMotor verticalslide;
    Servo jewelservo;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;
    Servo clawservo1;
    Servo clawservo2;
    CRServo crservo1;
    CRServo crservo2;

    public void runOpMode()throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        jewelservo = hardwareMap.servo.get("jewelservo");
        clawservo1 = hardwareMap.servo.get("rightclawservo");
        clawservo2 = hardwareMap.servo.get("leftclawservo");
        verticalslide = hardwareMap.dcMotor.get("verticalslide");
        crservo1 = hardwareMap.crservo.get("rightcrservo");
        crservo2 = hardwareMap.crservo.get("leftcrservo");


        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);
        verticalslide.setDirection(DcMotorSimple.Direction.REVERSE);

        verticalslide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        waitForStart();
        jewelservo.setPosition(.9);
        sleep(500);
        jewelservo.setPosition(.3);
        sleep(500);
        allclaws(.3, .4, .5, .6);
    }
    public void allclaws(double w,double x, double y, double z){
        clawservo1.setPosition(w);
        clawservo2.setPosition(x);

    }
}

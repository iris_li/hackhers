package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by Iris on 12/13/2017.
 */
//to test encoder values for the claw
@TeleOp (name="ClawPositionTest")
public class ClawPositionTest extends LinearOpMode{
    DcMotor clawmotor;
    public void runOpMode()throws InterruptedException{
        clawmotor = hardwareMap.dcMotor.get("clawmotor");
        clawmotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        clawmotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        waitForStart();
        while(opModeIsActive()) {
            int position = clawmotor.getCurrentPosition();
            telemetry.addData("Encoder Position", position);
            telemetry.update();

            if (gamepad2.left_bumper){
                clawmotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                clawmotor.setPower(-.1);
            } else if (gamepad2.right_bumper){
                clawmotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                clawmotor.setPower(.2);
            }else {
                clawmotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
        }
    }
}

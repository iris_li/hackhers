package org.firstinspires.ftc.teamcode.Parts;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Phoebe and Shih-Ting on 1/16/18.
 */
@TeleOp (name = "ClawServo")
@Disabled
public class Clawservos extends LinearOpMode{
    Servo clawservo1;
    Servo clawservo2;

    public void runOpMode()throws InterruptedException{
        clawservo1 = hardwareMap.servo.get("rightclawservo");
        clawservo2 = hardwareMap.servo.get("leftclawservo");

        waitForStart();
        while(opModeIsActive()) {
            //If right bumper is pressed, claw servo increased power.
            //If left bumper is pressed, claw servo increases power in the other direction
            //If no bumper is pressed, servo stops moving

            if(gamepad2.left_bumper){
                clawservo1.setPosition(.4);
                clawservo2.setPosition(.7);
            }else { 
                clawservo1.setPosition(.7);
                clawservo2.setPosition(.35);
            }
        }
    }
}




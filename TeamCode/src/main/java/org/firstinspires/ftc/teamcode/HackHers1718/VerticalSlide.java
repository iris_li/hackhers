package org.firstinspires.ftc.teamcode.HackHers1718;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/**
 * Created by Iris, Anna, Phoebe, and Sophia on 10/19/2017.
 */
@TeleOp (name = "VerticalSlide")
@Disabled
    public class VerticalSlide extends LinearOpMode{
        //declare motor
        DcMotor verticalslide;

        public void runOpMode() throws InterruptedException{
            verticalslide = hardwareMap.dcMotor.get("verticalslide");

            waitForStart();
            while(opModeIsActive()) {
                boolean verticalslidebutton = gamepad2.dpad_up;

                if (verticalslidebutton) {
                    verticalslide.setPower(.2);
                } else if(gamepad2.dpad_down){
                    verticalslide.setPower(-.2);
                } else {
                    verticalslide.setPower(0);
                }
            }
        }
    }

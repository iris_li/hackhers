package org.firstinspires.ftc.teamcode.Abstract;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

@Autonomous(name = "LiftTest")
public class LiftTest extends LinearOpMode{
    DcMotor lift;

    public void runOpMode() throws InterruptedException {
        lift = hardwareMap.dcMotor.get("lift");

        waitForStart();
        if(opModeIsActive()) {
            telemetry.addLine("Deploying from lander");
            sleep(500);
        }
    }
}

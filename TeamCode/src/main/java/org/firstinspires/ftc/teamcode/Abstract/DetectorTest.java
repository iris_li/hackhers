package org.firstinspires.ftc.teamcode.Abstract;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

@Autonomous(name= "DetectorTest")
@Disabled
public class DetectorTest extends LinearOpMode {
    Orientation lastAngles = new Orientation();
    GoldAlignDetector detector;
    double globalAngle, correction;
    RobotHardware robot;
    IMU imu;

    public void runOpMode() throws InterruptedException {
        detector = new GoldAlignDetector();
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        detector.useDefaults();

        robot = new RobotHardware(hardwareMap);
        imu = new IMU(hardwareMap);

        detector.enable();
        robot.setLiftPower(0);
        robot.setDropperPos(0);

        waitForStart();
        if (opModeIsActive()) {
            telemetry.addLine("Deploying from lander!");
            telemetry.update();
            //deploy the lander
            /*robot.setLiftPower(1);
            sleep(1000);
            robot.setLiftPower(0);
            driveStraight(.3, 500);
            robot.setLiftPower(-.6);
            sleep(1000);
            robot.setLiftPower(0);

            telemetry.addLine("Searching for gold...");
            telemetry.update();
            //start from the left
            rotate(-25);
            sleep(500);
            */
            if(detector.getAligned()) {
                telemetry.addLine("It works :)");
                telemetry.update();
                driveStraight(.5,1700);
                driveStraight(-.5,450);
                rotate(45);
                driveStraight(.5,1000);
                rotate(45);
                driveStraight(.5,1500);
            }else {
                telemetry.addLine("didn't work");
                telemetry.update();
                sleep(1000);
            }
            detector.disable();
        }
    }
    public double trackRotation(){
        Orientation angles = imu.getZYXAngles();
        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;
        //range is -180 to 180 (- is left, + is right)
        if (deltaAngle < -180) {
            deltaAngle += 360;
        }
        else if(deltaAngle > 180) {
            deltaAngle -= 360;
        }
        globalAngle = deltaAngle;
        //lastAngles = angles;
        return globalAngle;
    }
    public void resetAngle() {
        lastAngles = imu.getZYXAngles();
        globalAngle = 0;
    }
    //left +, right - vals
    public void rotate(double degrees){
        resetAngle();
        double delta = Math.abs(trackRotation());
        //double lastAngle = imu.getAngleZ();
        //double currentAngle = imu.getAngleZ();
        if(degrees > 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnLeft(.3);
                delta = Math.abs(trackRotation());
            }
        }else if(degrees < 0){
            while((delta < Math.abs(degrees)) && opModeIsActive()){
                robot.turnRight(.3);
                delta = Math.abs(trackRotation());
            }
        }
        lastAngles = imu.getZYXAngles();
        robot.drive(0);
    }
    //corrects driving in a straight line by adjustDirection's correction
    public void driveStraight(double power,long milliseconds){
        ElapsedTime timer = new ElapsedTime();
        timer.reset();
        resetAngle();
        while ((timer.milliseconds() < milliseconds) && opModeIsActive()) {
            correction = adjustDirection();
            robot.setRightMotorPwr(power);
            robot.setLeftMotorPwr(power + correction);
        }
        robot.drive(0);
    }
    //if the delta angle is 0, no correction. else, correction = -deltaAngle*gain (which could be pos or neg)
    public double adjustDirection() {
        double correction, angle, gain = 0.10;
        angle = trackRotation();
        if (angle == 0) {
            correction = 0;
        } else {
            correction = -angle;
        }
        correction = correction * gain;
        return correction;
    }
}

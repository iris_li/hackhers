package org.firstinspires.ftc.teamcode.Testing;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import java.util.Locale;

/*
 * This is a LinearOpMode that shows how to use
 * the REV Robotics Color-Distance Sensor.
 */

//for corner blue side
@Autonomous(name = "RevColorSensor", group = "Sensor")
@Disabled
public class RevColorSensor extends LinearOpMode {
    DcMotor motorFrontRight;
    DcMotor motorFrontLeft;
    DcMotor motorBackRight;
    DcMotor motorBackLeft;
    Servo jewelservo;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;

    @Override
    public void runOpMode()throws InterruptedException{

        /*
        int redupperbound = sensorColor.red();
        int greenupperbound = sensorColor.green();
        int blueupperbound = sensorColor.blue();
        int redlowerbound = sensorColor.red();
        int greenlowerbound = sensorColor.green();
        int bluelowerbound = sensorColor.blue();
        */

        motorFrontRight = hardwareMap.dcMotor.get("rightfront");
        motorFrontLeft = hardwareMap.dcMotor.get("leftfront");
        motorBackLeft = hardwareMap.dcMotor.get("leftback");
        motorBackRight = hardwareMap.dcMotor.get("rightback");
        jewelservo = hardwareMap.servo.get("jewelservo");

        motorFrontRight.setDirection(DcMotorSimple.Direction.REVERSE);
        motorBackRight.setDirection(DcMotorSimple.Direction.REVERSE);

        // get a reference to the color sensor.
        sensorColor = hardwareMap.get(ColorSensor.class, "sensor_color_distance");

        // get a reference to the distance sensor that shares the same name.
        sensorDistance = hardwareMap.get(DistanceSensor.class, "sensor_color_distance");

        // hsvValues is an array that will hold the hue, saturation, and value information.
        float hsvValues[] = {0F, 0F, 0F};

        // values is a reference to the hsvValues array.
        final float values[] = hsvValues;

        // sometimes it helps to multiply the raw RGB values with a scale factor
        // to amplify/attentuate the measured values.
        final double SCALE_FACTOR = 255;

        // get a reference to the RelativeLayout so we can change the background
        // color of the Robot Controller app to match the hue detected by the RGB sensor.
        int relativeLayoutId = hardwareMap.appContext.getResources().getIdentifier("RelativeLayout", "id", hardwareMap.appContext.getPackageName());
        final View relativeLayout = ((Activity) hardwareMap.appContext).findViewById(relativeLayoutId);

        waitForStart();

        // loop and read the RGB and distance data.
        while (opModeIsActive()) {
            // convert the RGB values to HSV values, multiply by the SCALE_FACTOR, then cast it back to int (SCALE_FACTOR is a double)
            Color.RGBToHSV((int) (sensorColor.red() * SCALE_FACTOR),
                    (int) (sensorColor.green() * SCALE_FACTOR),
                    (int) (sensorColor.blue() * SCALE_FACTOR),
                    hsvValues);

            // send the info back to driver station using telemetry function.
            telemetry.addData("Distance (cm)",
                    String.format(Locale.US, "%.02f", sensorDistance.getDistance(DistanceUnit.CM)));
            telemetry.addData("Alpha", sensorColor.alpha());
            telemetry.addData("Red  ", sensorColor.red());
            telemetry.addData("Green", sensorColor.green());
            telemetry.addData("Blue ", sensorColor.blue());
            telemetry.addData("Hue", hsvValues[0]);

            // change the background color to match the color detected by the RGB sensor.
            // pass a reference to the hue, saturation, and value array as an argument to the HSVToColor method.
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.HSVToColor(0xff, values));
                }
            });
            telemetry.update();

            jewelservo.setPosition(.1);
            /*
            if (sensorColor.red() < 70 && sensorColor.red() > 50 && hsvValues[0] > 300 && sensorColor.green() <30 && sensorColor.green() > 10 && sensorColor.blue() < 20 && sensorColor.blue() > 10){
                motorFrontRight.setPower(-.1);
                motorFrontLeft.setPower(-.1);
                motorBackLeft.setPower(-.1);
                motorBackRight.setPower(-.1);
                sleep(500);
            }else{
                motorFrontRight.setPower(0);
                motorFrontLeft.setPower(0);
                motorBackRight.setPower(0);
                motorBackLeft.setPower(0);
            }
            jewelservo.setPosition(.9);
            motorFrontRight.setPower(.3);
            motorFrontLeft.setPower(.3);
            motorBackLeft.setPower(.3);
            motorBackRight.setPower(.3);
            sleep(1337);
            motorFrontRight.setPower(-.3);
            motorFrontLeft.setPower(.3);
            motorBackLeft.setPower(-.3);
            motorBackRight.setPower(.3);
            sleep(1000);
            motorFrontRight.setPower(0);
            motorFrontLeft.setPower(0);
            motorBackLeft.setPower(0);
            motorBackRight.setPower(0);
            */
        }

        // Set the panel back to the default color
        relativeLayout.post(new Runnable() {
            public void run() {
                relativeLayout.setBackgroundColor(Color.WHITE);
            }
        });
    }
}

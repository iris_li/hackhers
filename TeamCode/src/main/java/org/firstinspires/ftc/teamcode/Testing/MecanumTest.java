package org.firstinspires.ftc.teamcode.Testing;

import static java.lang.Math.abs;
import static org.firstinspires.ftc.robotcore.external.navigation.NavUtil.scale;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by Iris on 11/14/2017.
 */
/*
@TeleOp (name="MecanumTest")
    public class MecanumTest extends OpMode {
        DcMotor leftfront;
        DcMotor leftback;
        DcMotor rightfront;
        DcMotor rightback;
        @Override
        public void init() {
            leftfront = hardwareMap.dcMotor.get("leftfront");
            leftback = hardwareMap.dcMotor.get("leftback");
            rightfront = hardwareMap.dcMotor.get("rightfront");
            rightback = hardwareMap.dcMotor.get("rightback");
            rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
            rightback.setDirection(DcMotorSimple.Direction.REVERSE);
        }
        @Override
        public void loop(){
            double Speed = -gamepad1.left_stick_y;
            double Turn = gamepad1.left_stick_x;
            double Strafe = gamepad1.right_stick_x;
            double MAX_SPEED = 1.0;
            holonomic(Speed, Turn, Strafe, MAX_SPEED);

        public void holonomic ( double Speed, double Turn, double Strafe, double MAX_SPEED){
        //      Left Front = +Speed + Turn - Strafe      Right Front = +Speed - Turn + Strafe
        //      Left Rear  = +Speed + Turn + Strafe      Right Rear  = +Speed - Turn - Strafe
            double Magnitude = abs(Speed) + abs(Turn) + abs(Strafe);
            Magnitude = (Magnitude > 1) ? Magnitude : 1; //Set scaling to keep -1,+1 range
            leftfront.setPower(scale((scaleInput(Speed) + scaleInput(Turn) - scaleInput(Strafe)),
                    -Magnitude, +Magnitude, -MAX_SPEED, +MAX_SPEED));
            if (leftback != null) {
                leftback.setPower(scale((scaleInput(Speed) + scaleInput(Turn) + scaleInput(Strafe)),
                        -Magnitude, +Magnitude, -MAX_SPEED, +MAX_SPEED));
                    }
            rightfront.setPower(scale((scaleInput(Speed) - scaleInput(Turn) + scaleInput(Strafe)),
                    -Magnitude, +Magnitude, -MAX_SPEED, +MAX_SPEED));
            if (rightfront != null) {
                rightfront.setPower(scale((scaleInput(Speed) - scaleInput(Turn) - scaleInput(Strafe)),
                        -Magnitude, +Magnitude, -MAX_SPEED, +MAX_SPEED));
                    }
             }
        }
        @Override
        public void stop(){
        }
    }
 */
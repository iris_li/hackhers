package org.firstinspires.ftc.teamcode.Testing;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

/**
 * Created by Iris on 1/17/2018.
 */
@TeleOp(name = "CRServo")
@Disabled
public class CRServoTest extends LinearOpMode{
    CRServo crservo1;
    CRServo crservo2;
    public void runOpMode() throws InterruptedException{
        crservo1 = hardwareMap.crservo.get("rightcrservo");
        crservo2 = hardwareMap.crservo.get("leftcrservo");

        waitForStart();
        while(opModeIsActive()){
            if(gamepad2.right_bumper) {
                crservo1.setPower(.1);
                crservo2.setPower(.6);
            }
            else{
                crservo1.setPower(.6);
                crservo2.setPower(.1);
            }
        }
    }
}

package org.firstinspires.ftc.teamcode.NotWorking;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.Abstract.Arm;
/**
 * Created by Iris on 2/13/2018.
 */
public abstract class CoreOp extends OpMode {
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    /*
    Servo bjoint;
    Servo mjoint;
    CRServo intake;
    CRServo tjoint;
    */
    //Arm arm;

    public void init(){
        //arm.init(hardwareMap);

        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");

        /*

        bjoint = hardwareMap.servo.get("bottom joint");
        mjoint = hardwareMap.servo.get("middle joint");
        tjoint = hardwareMap.crservo.get("top joint");
        intake = hardwareMap.crservo.get("intake");
        */

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        //sets motors to stop moving when they don't receive any inputs, prevents drifting
        rightfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    /*
    public void resetArm(){
        arm.pivot(0,bjoint);
        arm.pivot(.7,mjoint);
    }
    */
}

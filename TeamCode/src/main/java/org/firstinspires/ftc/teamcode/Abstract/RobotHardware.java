package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

public class RobotHardware {
    DcMotor rf, lf, rb, lb, lift;
    Servo dropper;
    private HardwareMap hardwareMap;

    public RobotHardware(HardwareMap hardwareMap){
        this.hardwareMap = hardwareMap;
        rf = this.hardwareMap.dcMotor.get("rightfront");
        lf = this.hardwareMap.dcMotor.get("leftfront");
        rb = this.hardwareMap.dcMotor.get("rightback");
        lb = this.hardwareMap.dcMotor.get("leftback");
        lift = this.hardwareMap.dcMotor.get("lift");
        dropper = this.hardwareMap.servo.get("dropper");

        rf.setDirection(DcMotorSimple.Direction.REVERSE);
        rb.setDirection(DcMotorSimple.Direction.REVERSE);

        rf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    public void drive(double power){
        rf.setPower(power);
        rb.setPower(power);
        lf.setPower(power);
        lb.setPower(power);
    }
    public void strafeRight(double power){
        rf.setPower(-power);
        rb.setPower(power);
        lf.setPower(power);
        lb.setPower(-power);
    }
    public void strafeLeft(double power){
        rf.setPower(power);
        rb.setPower(-power);
        lf.setPower(-power);
        lb.setPower(power);
    }
    public void turnRight(double power){
        rf.setPower(-power);
        rb.setPower(-power);
        lf.setPower(power);
        lb.setPower(power);
    }
    public void turnLeft(double power){
        rf.setPower(power);
        rb.setPower(power);
        lf.setPower(-power);
        lb.setPower(-power);
    }
    public void setLeftMotorPwr(double power){
        lf.setPower(power);
        lb.setPower(power);
    }
    public void setRightMotorPwr(double power){
        rf.setPower(power);
        rb.setPower(power);
    }
    public void setDropperPos(double power){
        dropper.setPosition(power);
    }
    public void setMotorPowers(double rfPower, double rbPower, double lfPower, double lbPower){
        rf.setPower(rfPower);
        rb.setPower(rbPower);
        lf.setPower(lfPower);
        lb.setPower(lbPower);
    }
    public void setLiftPower(double power){
        lift.setPower(power);
    }
}

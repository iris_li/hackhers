package org.firstinspires.ftc.teamcode.Abstract;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

public class Bumper {
    HardwareMap hardware;

    public void init(HardwareMap hardware) {
        Servo bumper;
        bumper = hardware.servo.get("bumper");
        pivot(0, bumper);
    }

    public void pivot(double position, Servo servo) {
        while (servo.getPosition() < position) {
            servo.setPosition(servo.getPosition() + (position * .1));
        }
        while (servo.getPosition() > position) {
            servo.setPosition(servo.getPosition() - (position * .1));
        }
    }
    public void pivot(boolean posdir, Servo servo) {
        if (posdir) {
            servo.setPosition(servo.getPosition() + .1);
        } else if (!posdir) {
            servo.setPosition(servo.getPosition() - .1);
        }
    }
}
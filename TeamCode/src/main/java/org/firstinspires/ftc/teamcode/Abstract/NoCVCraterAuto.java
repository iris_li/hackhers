package org.firstinspires.ftc.teamcode.Abstract;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.AutonomousTransitionSamples.AutoTransitioner;

/**
 * Blue autonomous for lowering robot and parking in the crater, imaging and servos coming soon
 */
@Autonomous (name="NoCVCraterAuto")

public class NoCVCraterAuto extends LinearOpMode {
    // gotta have motors
    DcMotor rightfront;
    DcMotor leftfront;
    DcMotor rightback;
    DcMotor leftback;
    DcMotor lift;
    Servo dropper;

    public void runOpMode()throws InterruptedException {
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        lift = hardwareMap.dcMotor.get("lift");

        dropper = hardwareMap.servo.get("dropper");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        //sets motors to stop moving when they don't receive any inputs, prevents drifting
        rightfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftfront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightback.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // this autonomous does not use computer vision, it just goes for the middle particle
        lift.setPower(0);
        dropper.setPosition(0);
        AutoTransitioner.transitionOnStop(this,"BasicTeleopDrive");
        waitForStart();
        if(opModeIsActive()) {
            // sets dropper position
            dropper.setPosition(.0);
            // lower robot
            /*telemetry.addLine("lowering robot");
            telemetry.update();
            lift(-.8,1000);
            lift(.4,200);
            */
            // go to depot
            telemetry.addLine("Moving toward depot");
            telemetry.update();
            drive(.5,1500);
            drive(-.5,450);
            //sleep(500);
            turnLeft(.5,1100);
            drive(.6,2200);
            turnLeft(.5,300);
            drive(.6,1000);
            drive(0,500);
            // drop the marker now
            telemetry.addLine("Claiming depot");
            telemetry.update();
            dropper.setPosition(.45);
            sleep(1000);
            dropper.setPosition(0);
            sleep(500);
            // now head to crater :)
            telemetry.addLine("Parking in crater");
            telemetry.update();
            drive(-.7,2500);
            turnLeft(.4,600);
            drive(-.8,1000);
        }
    }

    //basic functions to be used
    public void drive(double power, long time) {
        leftfront.setPower(power);
        leftback.setPower(power);
        rightfront.setPower(power);
        rightback.setPower(power);
        sleep(time);
    }
    public void turnLeft(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(-power);
        leftfront.setPower(power);
        leftback.setPower(power);
        sleep(time);
    }
    public void turnRight(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(power);
        leftfront.setPower(-power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeRight(double power, long time){
        rightfront.setPower(-power);
        rightback.setPower(power);
        leftfront.setPower(power);
        leftback.setPower(-power);
        sleep(time);
    }
    public void strafeLeft(double power, long time){
        rightfront.setPower(power);
        rightback.setPower(-power);
        leftfront.setPower(-power);
        leftback.setPower(power);
        sleep(time);
    }
    public void turnRight(double power){
        rightfront.setPower(power);
        rightback.setPower(-power);
        leftfront.setPower(-power);
        leftback.setPower(power);
    }
    public void lift(double power, long time){
        lift.setPower(power);
        sleep(time);
    }
}
